package cl.ceisufro.exoplayervideoview;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.File;
import java.util.ArrayList;

/**
 * Creado por Luis Andrés Jara Castillo on 15-03-18.
 */

public class ExoPlayerVideoView extends PlayerView {
    private DefaultBandwidthMeter mDefaultBandwidthMeter;
    private DefaultDataSourceFactory mDefaultDataSourceFactory;
    private ExoPlayer mExoPlayer;
    private PlayerListener playerListener;
    private ArrayList<Uri> mVideoSources;
    private int mVideoIndex;
    private boolean autoPlay;
    private boolean playWhenReady;
    private boolean paused;
    private boolean preparing;

    public ExoPlayerVideoView(Context context) {
        super(context);
        initView();
    }

    public ExoPlayerVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public ExoPlayerVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public PlayerListener getPlayerListener() {
        return playerListener;
    }

    public void setPlayerListener(PlayerListener playerListener) {
        this.playerListener = playerListener;
    }

    public boolean isAutoPlay() {
        return autoPlay;
    }

    public void setAutoPlay(boolean autoPlay) {
        this.autoPlay = autoPlay;
    }

    public boolean isPlayWhenReady() {
        return playWhenReady;
    }

    public boolean isPaused() {
        return paused;
    }

    public void setPlayWhenReady(boolean playWhenReady) {
        this.playWhenReady = playWhenReady;
    }

    private Player.EventListener mPlayerEventListener = new Player.DefaultEventListener() {
        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            super.onPlayerStateChanged(playWhenReady, playbackState);
            switch (playbackState) {
                case Player.STATE_READY: {
                    if (playerListener != null) {
                        if (playWhenReady) {
                            preparing = false;
                            paused = false;
                            playerListener.onStarted(mVideoIndex);
                        } else if (preparing) {
                            preparing = false;
                            playerListener.onReady(mVideoIndex);
                        } else {
                            paused = true;
                            long position = getPlaybackPosition();
                            playerListener.onPause(position);
                        }
                    } else {
                        resumePlayback();
                    }
                    break;
                }
                case Player.STATE_ENDED: {
                    if (playerListener != null)
                        playerListener.onCompletion(mVideoIndex, mVideoSources.size());
                    addUpIndex();
                    if (autoPlay) {
                        prepare();
                    } else {
                        releaseMediaPlayer();
                    }
                    break;
                }
            }
        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
            super.onPlayerError(error);
            if (playerListener != null)
                playerListener.onError(error);
        }

        @Override
        public void onSeekProcessed() {
            super.onSeekProcessed();
            if (playerListener != null)
                playerListener.onSeekProcessed();
        }
    };

    private void initView() {
        mDefaultBandwidthMeter = new DefaultBandwidthMeter();
        mDefaultDataSourceFactory = new DefaultDataSourceFactory(getContext(),
                Util.getUserAgent(getContext(), cl.ceisufro.exoplayervideoview.Util.getApplicationName(getContext())), mDefaultBandwidthMeter);
        mVideoSources = new ArrayList<>();
        mVideoIndex = 0;
        autoPlay = false;
        playWhenReady = false;
    }

    public void clearVideoSources(boolean stopPlayback) {
        if (this.mVideoSources != null)
            this.mVideoSources.clear();
        if (stopPlayback)
            stopPlayback();
    }

    public void setVideoSources(ArrayList<Uri> videoUris) {
        if (videoUris == null)
            throw new IllegalArgumentException("Sources list cannot be null or empty!");
        this.mVideoSources = null;
        this.mVideoSources = videoUris;
    }

    public ArrayList<Uri> getmVideoSources() {
        return mVideoSources;
    }

    public int getVideoCount() {
        int count = 0;
        if (mVideoSources != null)
            count = mVideoSources.size();
        return count;
    }

    public void addVideo(Uri uri) {
        if (uri == null)
            throw new IllegalArgumentException("Uri cannot be null!");
        if (mVideoSources == null)
            mVideoSources = new ArrayList<>();
        this.mVideoSources.add(uri);
    }

    public void addVideo(String videoPath) {
        if (TextUtils.isEmpty(videoPath))
            throw new IllegalArgumentException("Path cannot be null or empty!");
        addVideo(Uri.parse(videoPath));
    }

    public void addVideo(File file) {
        if (file == null || TextUtils.isEmpty(file.getPath()))
            throw new IllegalArgumentException("File is null or its path is empty!");
        addVideo(Uri.parse(file.getPath()));
    }

    public boolean removeVideo(int position, boolean stopPlayback) {
        if (mVideoSources == null)
            throw new IndexOutOfBoundsException("Sources list is null!");
        if (position >= mVideoSources.size())
            return false;
        if (stopPlayback)
            stopPlayback();
        return (this.mVideoSources.remove(position) != null);
    }

    public boolean removeVideo(Uri uri, boolean stopPlayback) {
        if (mVideoSources == null)
            throw new NullPointerException("Sources list is null!");
        if (uri == null)
            throw new NullPointerException("Parameter Uri is null!");
        if (stopPlayback)
            stopPlayback();
        return this.mVideoSources.remove(uri);
    }

    public int indexOf(Uri uri) {
        if (mVideoSources == null)
            throw new NullPointerException("Sources list is null!");
        if (uri == null)
            throw new NullPointerException("Parameter Uri is null!");
        return mVideoSources.indexOf(uri);
    }

    public int indexOf(File file) {
        if (mVideoSources == null)
            throw new NullPointerException("Sources list is null!");
        if (file == null || TextUtils.isEmpty(file.getPath()))
            throw new NullPointerException("File is null or its path is empty!");
        return mVideoSources.indexOf(Uri.parse(file.getPath()));
    }

    public int indexOf(String path) {
        if (mVideoSources == null)
            throw new NullPointerException("Sources list is null!");
        if (TextUtils.isEmpty(path))
            throw new NullPointerException("File path is null or empty!");
        return mVideoSources.indexOf(Uri.parse(path));
    }

    private void attachPlayerToView() {
        setPlayer(mExoPlayer);
    }

    private void initVideoPlayer() {
        try {
            if (mExoPlayer != null) {
                releaseMediaPlayer();
            }
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(mDefaultBandwidthMeter);
            TrackSelector trackSelector =
                    new DefaultTrackSelector(videoTrackSelectionFactory);
            mExoPlayer = ExoPlayerFactory.newSimpleInstance(getContext(), trackSelector);
            mExoPlayer.addListener(mPlayerEventListener);
            attachPlayerToView();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void releaseMediaPlayer() {
        try {
            if (mExoPlayer != null) {
                mExoPlayer.release();
                mExoPlayer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void prepare(int position) {
        try {
            if (mVideoSources == null || position >= mVideoSources.size())
                throw new IndexOutOfBoundsException("The given position is out of bounds!");
            mVideoIndex = position;
            prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void prepare() {
        preparing = true;
        if (mExoPlayer == null)
            initVideoPlayer();
        mExoPlayer.setPlayWhenReady(playWhenReady);
        if (mVideoSources != null && !mVideoSources.isEmpty()) {
            mExoPlayer.prepare(generateMediaSource());
        } else {
            stopPlayback();
            mVideoIndex = 0;
        }
    }

    public void prepare(Uri mediaUri, boolean addToPlaylist) {
        try {
            preparing = true;
            if (mExoPlayer == null)
                initVideoPlayer();
            mExoPlayer.setPlayWhenReady(playWhenReady);
            mExoPlayer.prepare(generateMediaSource(mediaUri));
            if (addToPlaylist) {
                addVideo(mediaUri);
                mVideoIndex = mVideoSources.indexOf(mediaUri);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopPlayback() {
        try {
            if (mExoPlayer != null) {
                mExoPlayer.stop(true);
                long position = getPlaybackPosition();
                releaseMediaPlayer();
                if (playerListener != null)
                    playerListener.onStopped(position);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pausePlayback() {
        try {
            if (mExoPlayer != null) {
                mExoPlayer.setPlayWhenReady(false);
                long position = getPlaybackPosition();
                paused = true;
                if (playerListener != null)
                    playerListener.onPause(position);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resumePlayback() {
        try {
            if (mExoPlayer != null && mExoPlayer.getPlaybackState() == Player.STATE_READY) {
                mExoPlayer.setPlayWhenReady(true);
                paused = true;
                if (playerListener != null)
                    playerListener.onStarted(mVideoIndex);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long getPlaybackPosition() {
        long position = 0;
        if (mExoPlayer != null)
            position = mExoPlayer.getCurrentPosition();
        return position;
    }

    private MediaSource generateMediaSource() {
        Uri uri = mVideoSources.get(mVideoIndex);
        if (uri == null)
            throw new NullPointerException("Couldn't generate the media source. The uri in the list couldn't be processed or the uri was null!");
        return generateMediaSource(uri);
    }

    private MediaSource generateMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(mDefaultDataSourceFactory).createMediaSource(uri);
    }

    private void addUpIndex() {
        int max = mVideoSources != null ? (mVideoSources.size() - 1) : 0;
        mVideoIndex = mVideoIndex >= max ? 0 : (mVideoIndex + 1);
    }

    public void seekToPosition(long positionInMillis) {
        try {
            if (mExoPlayer != null)
                mExoPlayer.seekTo(positionInMillis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
