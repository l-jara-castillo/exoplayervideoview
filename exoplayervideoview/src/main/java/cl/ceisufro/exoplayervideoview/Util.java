package cl.ceisufro.exoplayervideoview;

import android.content.Context;
import android.content.pm.ApplicationInfo;

/**
 * Creado por Luis Andrés Jara Castillo on 16-03-18.
 */

class Util {
    static String getApplicationName(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
    }
}
