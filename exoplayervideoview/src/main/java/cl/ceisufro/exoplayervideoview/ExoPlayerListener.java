package cl.ceisufro.exoplayervideoview;

import com.google.android.exoplayer2.ExoPlaybackException;

/**
 * Creado por Luis Andrés Jara Castillo on 27-03-18.
 */

public class ExoPlayerListener implements PlayerListener {
    @Override
    public void onError(ExoPlaybackException exoPlaybackException) {

    }

    @Override
    public void onPause(long positionInMillis) {

    }

    @Override
    public void onReady(int itemIndex) {

    }

    @Override
    public void onStarted(int itemIndex) {

    }

    @Override
    public void onStopped(long positionInMillis) {

    }

    @Override
    public void onCompletion(int itemIndexInList, int totalItems) {

    }

    @Override
    public void onSeekProcessed() {

    }
}
