package cl.ceisufro.exoplayervideoview;

import com.google.android.exoplayer2.ExoPlaybackException;

/**
 * Creado por Luis Andrés Jara Castillo on 15-03-18.
 */

interface PlayerListener {
    void onError(ExoPlaybackException exoPlaybackException);

    void onPause(long positionInMillis);

    void onReady(int itemIndex);

    void onStarted(int itemIndex);

    void onStopped(long positionInMillis);

    void onCompletion(int itemIndexInList, int totalItems);

    void onSeekProcessed();
}
